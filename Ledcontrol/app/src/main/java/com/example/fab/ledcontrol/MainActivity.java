package com.example.fab.ledcontrol;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    Button sendButton;

    SeekBar greenSB, redSB, blueSB;
    int seekR, seekB, seekG;
    final String LOG_TAG = "myLogs";
    RelativeLayout mainScreen;
    BluetoothAdapter btAdapter;
    BluetoothSocket btSocket;

   // byte[] address;
    public static final String MAC_ADDRESS = "20:15:06:19:11:96";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sendButton = (Button) findViewById(R.id.buttonSend);
        sendButton.setOnClickListener(this);
        greenSB = (SeekBar) findViewById(R.id.seekBarG);
        redSB = (SeekBar) findViewById(R.id.seekBarR);
        blueSB = (SeekBar) findViewById(R.id.seekBarB);

        greenSB.setOnSeekBarChangeListener(seekBarChangeListener);
        redSB.setOnSeekBarChangeListener(seekBarChangeListener);
        blueSB.setOnSeekBarChangeListener(seekBarChangeListener);

        mainScreen = (RelativeLayout) findViewById(R.id.screen);

        String enableBT = BluetoothAdapter.ACTION_REQUEST_ENABLE;
        startActivityForResult(new Intent(enableBT), 0);

        btAdapter = BluetoothAdapter.getDefaultAdapter();

        updateBack();

        try{

            BluetoothDevice device = btAdapter.getRemoteDevice(MAC_ADDRESS);

            //Инициируем соединение с устройством
            Method m = device.getClass().getMethod(
                    "createRfcommSocket", new Class[] {int.class});

            btSocket = (BluetoothSocket) m.invoke(device, 1);
            btSocket.connect();

            //В случае появления любых ошибок, выводим в лог сообщение
        } catch (IOException e) {
            Log.d("BLUETOOTH", e.getMessage());
        } catch (SecurityException e) {
            Log.d("BLUETOOTH", e.getMessage());
        } catch (NoSuchMethodException e) {
            Log.d("BLUETOOTH", e.getMessage());
        } catch (IllegalArgumentException e) {
            Log.d("BLUETOOTH", e.getMessage());
        } catch (IllegalAccessException e) {
            Log.d("BLUETOOTH", e.getMessage());
        } catch (InvocationTargetException e) {
            Log.d("BLUETOOTH", e.getMessage());
        }

        //Выводим сообщение об успешном подключении
        Toast.makeText(getApplicationContext(), "CONNECTED", Toast.LENGTH_LONG).show();





    }






   /* public void onClick(View v) {

        //Пытаемся послать данные
        try {
            //Получаем выходной поток для передачи данных
            OutputStream outStream = btSocket.getOutputStream();

            int value = 0;


            //Пишем данные в выходной поток
            outStream.write(value);

        } catch (IOException e) {
            //Если есть ошибки, выводим их в лог
            Log.d("BLUETOOTH", e.getMessage());
        }
    } */

    private SeekBar.OnSeekBarChangeListener seekBarChangeListener =
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,
                                               boolean fromUser){
                    // TODO Auto-generated method stub
                    updateBack();
                }

                public void onStartTrackingTouch(SeekBar seekBar){
                    // TODO Auto-generated method stub
                }

                public void onStopTrackingTouch(SeekBar seekBar){
                    // TODO Auto-generated method stub
                }
            };


    private void updateBack(){
        seekB = blueSB.getProgress();
        seekR = redSB.getProgress();
        seekG = greenSB.getProgress();

        mainScreen.setBackgroundColor(0xff000000 + seekR * 0x10000 + seekG * 0x100
                + seekB);
    }


    @Override
    public void onClick(View v) {

    }
}
